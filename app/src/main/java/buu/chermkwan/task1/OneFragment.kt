package buu.chermkwan.task1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import buu.chermkwan.task1.databinding.FragmentOneBinding
import kotlinx.android.synthetic.main.fragment_one.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [OneFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OneFragment : Fragment() {
    private lateinit var binding:FragmentOneBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentOneBinding>(inflater, R.layout.fragment_one, container, false)
        game()
        return binding.root
    }
    private var correctScore: Int = 0
    private var incorrectScore: Int = 0

    private fun game() {
        binding.apply {
            var randomNum1 = Random.nextInt(0, 10)
            var randomNum2 = Random.nextInt(0, 10)
            val randomBtn = Random.nextInt(0, 3)

            txtNum1.text = randomNum1.toString()
            txtNum2.text = randomNum2.toString()

            var a = Random.nextInt(1, 18)
            var b = Random.nextInt(1, 18)

            val answer = randomNum1 + randomNum2

            if (a == b) {
                a++
                b--
            } else if (answer == a) {
                a++
            } else if (answer == b) {
                b++
            }

            if (randomBtn == 1) {
                btnAns1.text = answer.toString()
                btnAns2.text = a.toString()
                btnAns3.text = b.toString()
            } else if (randomBtn == 2) {
                btnAns2.text = answer.toString()
                btnAns1.text = b.toString()
                btnAns3.text = a.toString()
            } else {
                btnAns3.text = answer.toString()
                btnAns1.text = a.toString()
                btnAns2.text = b.toString()
            }

            btnAns1.setOnClickListener {
                if (btnAns1.text.toString() == answer.toString()) {
                    txtTorF.text = "Correct"
                    correctScore++
                    txtC.text = correctScore.toString()
                    game()
                } else {
                    txtTorF.text = "Incorrect"
                    incorrectScore++
                    txtIn.text = incorrectScore.toString()
                    game()
                }
            }
            btnAns2.setOnClickListener {
                if (btnAns2.text.toString() == answer.toString()) {
                    txtTorF.text = "Correct"
                    correctScore++
                    txtC.text = correctScore.toString()
                    game()
                } else {
                    txtTorF.text = "Incorrect"
                    incorrectScore++
                    txtIn.text = incorrectScore.toString()
                    game()
                }
            }
            btnAns3.setOnClickListener {
                if (btnAns3.text.toString() == answer.toString()) {
                    txtTorF.text = "Correct"
                    correctScore++
                    txtC.text = correctScore.toString()
                    game()
                } else {
                    txtTorF.text = "Incorrect"
                    incorrectScore++
                    txtIn.text = incorrectScore.toString()
                    game()
                }
            }
        }

    }

    }